var app = angular.module('short', []);
	app.factory('cut', ['$http', '$q',function($http, $q) {
		return {
			create:	function(url, short){
				var deferred = $q.defer();

				$http.post('/api/create', { url: url, short: short })
					.success(function(data) {
						deferred.resolve(data);
					})
					.error(function() {
						deferred.reject();
					});
				
				return deferred.promise;
			}
		}
	}]);

	app.controller('main', ['$scope', '$location', 'cut', function($scope, $location, cut) {
		var re		= /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:ww‌​w.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?‌​(?:[\w]*))?)/;

		$scope.cut	= function() {
			$scope.state	= '';
			$scope.link		= '';

			if ($scope.url.length) {
				if (re.test($scope.url)) {
					cut.create($scope.url, $scope.short)
						.then(function(result) {
							$scope.state	= 'Success!';	
							$scope.short	= result.short;
							$scope.link		= $location.absUrl() + result.short;
						}, function() {
							$scope.state = 'Oops! Something going wrong :(';	
						});
				}
				else
					alert('Please, input correct URL.')
			}
			else
				alert('Please, input URL.')
		}
	}]);
