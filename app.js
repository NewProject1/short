var express		= require('express');
var request		= require('request');
var bodyParser	= require('body-parser');
var redis		= require('redis');
var md5			= require('md5');
var config		= require('nconf');
var cors		= require('cors');
var client		= redis.createClient();
var app			= express();

config.file('./config.json');

config.defaults({
	'http': {
		'port': 8080
	}
});

// Allow cross-domain requests 
// e.g POST { url: 'http://upwork.com', short: 'xxx' } to /api/create
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json()); 

app.use('/js', express.static(__dirname + '/js'));
app.use('/css', express.static(__dirname + '/css'));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.post('/api/create', function(req, res) {
	var url		= req.body.url;	
	var surl	= req.body.short;	

	if (url) {
		request(url, function(err, response) {
			if (err || response.statusCode != 200)
				res.sendStatus(500);
			else {
				var cut = function(url) {
					return  md5(url + +new Date).replace(/[\d]+/g, '');
				};

				if (!surl || !surl.length) 
					surl = cut(url);

				var check	= function(surl) {
					client.hgetall('url:'+surl, function(err, data) {
						if (data) 
							check(cut(url));
						else {
							client.hset('url:' + surl, 'url', url, function(err) {
								if (!err) {
									client.hset('url:' + surl, 'count', 0, function(err) {
										if (!err) 
											res.status(200).json({ url: url, short: surl });
										else
											res.sendStatus(500);
									});
								}
								else
									res.sendStatus(500);
							});
						}
					});
				}

				check(surl);
			}
		});
	}
	else
		res.sendStatus(500);
});

app.get(/.+/, function(req, res) {
	var url = req.originalUrl.replace('/', '');
	client.hgetall('url:' + url, function(err, data) {
		if (!err && data && data.url) {
			data.count++;

			client.hset('url:' + url, 'count', data.count, function(err) {
				if (!err) {
					console.log(data.count);
					res.redirect(data.url);
				}
				else
					res.sendStatus(404);
			});
		}
		else
			res.sendStatus(404);
	})
});

var server	= app.listen(config.get('http:port'), function() {
	console.log('Started')	
});
